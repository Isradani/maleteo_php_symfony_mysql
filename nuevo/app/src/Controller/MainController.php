<?php

namespace App\Controller;

use App\Entity\Comments;
use App\Entity\UserDemo;
use App\Form\FormularioCommentsType;
use App\Form\UsuariosForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */

    /*Asi paso variables a la plantilla de twig ( variable name = isra) 
     1º render 
     2º la plantilla twig 
     3º la variable

    public function index()
    {
        return $this->render('base.html.twig', ['name' => 'isra']);
    }
*/

    /**
     * @Route("/lista", name="listar_usuarios")
     */

    public function listarUsuarios(EntityManagerInterface $em)
    {

        $repositorio =  $em->getRepository(UserDemo::class);
        $usuarios = $repositorio->findAll();

        return $this->render(
            'listaUsuarios.html.twig',
            [
                'usuarios' => $usuarios
            ]
        );
    }

    /**
     * @Route("/", name="homepage")
     */
    public function nuevoUsuario(Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(UsuariosForm::class);

        $form->handleRequest($request);

        // $comments_file = __DIR__ . "/../../public/data/comments.json";

        // $comments = json_decode(file_get_contents($comments_file));

        $repositorio =  $em->getRepository(Comments::class);
        $comments = $repositorio->findAll();
        

        if ($form->isSubmitted() && $form->isValid()) {
            $usuarios = $form->getData();
            $em->persist($usuarios);
            $em->flush();
       
            // dd($usuarios);

            return $this->redirectToRoute('ok_formulario');
        }

        
        return $this->render(
            'maleteo.html.twig',
            [
                'formulario' => $form->createView(),
                
                'comments' => $comments
                
            ]
        );

    }

    /**
     * @Route("/usuarios", name="ok_formulario")
     */
     public function okFormulario(EntityManagerInterface $em)
    {
        //  $comments_file = __DIR__ . "/../../public/data/comments.json";
        //  $comments = json_decode(file_get_contents($comments_file));

        $repositorio =  $em->getRepository(Comments::class);
        $comments = $repositorio->findAll();

         return $this->render(
             'okMaleteo.html.twig',
            [
                 'comments' => $comments
                
             ]
         );
     }

    /**
     * @Route("/comentarios", name="Comentarios")
     */
    public function comentarios(Request $request, EntityManagerInterface $em){

        $comentarios = $this->createForm(FormularioCommentsType::class);

        $comentarios->handleRequest($request);

        if ($comentarios->isSubmitted() && $comentarios->isValid()) {

            $comentario = $comentarios->getData();
           $em->persist($comentario);
            $em->flush();
       
            //dd($comentario);

            return $this->redirectToRoute('homepage');
        }


        return $this->render(
            'comentarios.html.twig',
            [
                'comentarios' => $comentarios->createView(),
                
              
                
            ]
        );
    }
}
