<?php

namespace App\Form;
use App\Entity\UserDemo;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuariosForm extends AbstractType{

    public function buildForm(\Symfony\Component\Form\FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre');
        $builder->add('apellidos');
        $builder->add('email');
        $builder->add('horario');
        $builder->add('ciudad', ChoiceType::class,
    [
        'choices' => [
            'Barcelona' => 'Barcelona',
            'Sevilla' => 'Sevilla',
            'Ceuta' => 'Ceuta',
            'Malaga' => 'Malaga',
            'Badajoz' => 'Badajoz',
        ]
    ]);
    }
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            
        [
            'data_class' => UserDemo::class
        ]
            
        );
    }
}

?>

