<?php

namespace App\Repository;

use App\Entity\UserDemo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserDemo|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserDemo|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserDemo[]    findAll()
 * @method UserDemo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserDemoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserDemo::class);
    }

    // /**
    //  * @return UserDemo[] Returns an array of UserDemo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserDemo
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
